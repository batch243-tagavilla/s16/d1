// console.log("Hello World");

// [Sections] Arithmetic Operators

    let x = 1397;
    let y = 7831;

    console.log("The value of x is " + x);
    console.log("The value of y is " + y);

    // Addition operator
    let sum = x + y;
    console.log("Result of addition operator: " + sum);

    // Difference operator
    let difference = x - y;
    console.log("Result of subtraction operator: " + difference);
    
    // Multiplication operator
    let product = x * y;
    console.log("Result of multiplication operator: " + product);

    // Division operator
    let quotient = x / y;
    console.log("Result of division operator: " + quotient);

    // Modulo
    let remainder = y % x;
    console.log("Result of modulo: " + remainder);

// [Section] Assignment Operators

    // Basic assignment operator (=) - adds the value of the right operand to a variable and assigns the result to the value.
    let assignmentNumber = 8;
    console.log(assignmentNumber);

    // Addition assignment operator (+=) - adds the value of the right operand to a variable and assigns the result to the variable.
    console.log("Result of addition assignment operator: " + (assignmentNumber+=2));

    // Subtraction assignment operator (-=) - subtracts the value of the right operand to a variable and assigns the result to the variable.
    console.log("Result of subtraction assignment operator: " + (assignmentNumber-=2));

    // Multiplication assignment operator (*=) - multiplies the value of the right operand to a variable and assigns the result to the variable.
    console.log("Result of multiplication assignment operator: " + (assignmentNumber*=4));

    // Division assignment operator (/=) - multiplies the value of the right operand to a variable and assigns the result to the variable.
    console.log("Result of division assignment operator: " + (assignmentNumber/=8));

// [Section] Multiple Operators and Parenthesss

    /* 
        1. 3*4= 12
        2. 12/5= 2.4
        3. 1+2= 3
        4. 3-2.4 = 0.6
    */

    let mdas = 1 + 2 - 3 * 4 / 5;
    console.log("Result of mdas rule: " + mdas);

    let pemdas = 1 + (2 - 3) * (4 / 5);
    console.log("Result of pemdas rule: " + pemdas);

// [Section] Incremention and Decrementation

    // Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to.
    let z = 1;
    console.log("Value of z is " + z);

    // Pre-increment z=1
    console.log("Result of pre-increment is " + ++z); // z=2

    // Post-increment
    console.log("Result of post-increment is " + z++); //z=2
    // z=3

    // Pre-decrement
    console.log("Result of pre-increment is " + --z); //z=2

    // Post-decrement
    console.log("Result of post-increment is " + z--); //z=2
    // z=1

// [Section] Type Coercion

    /* 
        - Type coercion is the automatic or implicit conversion of values from one data type to another.
        - This happens when operations performed on different data types that would normally not be possible and yield irregular results.
        - Values are automatically converted from one data type to another in order to resolve operations.
    */

        let numA = "10";
        let numB = 12;

        // adding or concatinating a string and a number will result into string
        console.log(numA + numB);
        console.log(typeof (numA + numB));

        // non-coercion
        let numC = 16;
        let numD = 14;
        console.log(numC + numD);
        console.log(typeof (numC + numD));

        // boolean value and number = number
        // true = 1,  false = 0
        let numE = false + 1;
        console.log(numE);

// [Section] Comparison Operators 

    let juan = "juan";

    // Equality operator (==) - checks whether the operands are equal / have the same value
    console.log("Equality Operator")
    console.log(1 == 1);
    console.log(typeof (1 == 1));
    console.log("juan" == juan);

    // Strict equality operator (===) - checks whether the operands are equal / have the same value and same data type
    console.log("Strict Equality Operator")
    console.log(1 === "1");

    // Inequality operator (!=) - checks whether the operands are not equal / have different content
    console.log("Inequality Operator")
    console.log(1!='1');

    // Strict inequality operator (===)
    console.log("Strict Inequality Operator")
    console.log(1!=='1');

// [Section] Relational Operators

    // Some comparison operators check whether one value is greater or less than to the other value.

    let a = 50;
    let b = 65;

    // Greater than operator (>)
    console.log("Greater than");
    console.log(a>b);

    // Less than operator (<)
    console.log("Less than");
    console.log(a<b);

    // Greater than or Equal operator (>=)
    console.log("Greater than or Equal than");
    console.log(a>=b);

    // Less than or Equal operator (<=)
    console.log("Less than or Equal than");
    console.log(a<=b);

    let numStr = "30";
    console.log(a>numStr);
    console.log(b<=numStr);

// [Section] Logical Operators

    let isLegalAge = true;
    let isRegistered = false;

    // Logical And (&&)
    console.log("Result of logical And: " + (isLegalAge && isRegistered));

    // Logical Or (||)
    console.log("Result of logical Or: " + (isLegalAge || isRegistered));

    // Logical Not (!)
    console.log("Result of logical Not: " + !(isLegalAge && isRegistered));